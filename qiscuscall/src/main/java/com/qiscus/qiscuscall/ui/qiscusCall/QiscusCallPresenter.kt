package com.qiscus.qiscuscall.ui.qiscusCall

import com.nexmo.client.NexmoCall
import com.nexmo.client.NexmoCallHandler
import com.nexmo.client.NexmoClient
import com.nexmo.client.NexmoUser
import com.nexmo.client.request_listener.NexmoApiError
import com.nexmo.client.request_listener.NexmoRequestListener
import com.qiscus.qiscuscall.currentCall


class QiscusCallPresenter(
    val view: QiscusCallView,
    private val nexmoClient: NexmoClient
) {
    fun login(token: String) {
        nexmoClient.login(token, object : NexmoRequestListener<NexmoUser?> {
            override fun onSuccess(p0: NexmoUser?) {
                view.loginSuccess(p0)
            }

            override fun onError(p0: NexmoApiError) {
                view.lonError(p0)
            }
        })
    }

    fun call(username: String) {
        if (currentCall == null) {
            nexmoClient.call(
                username,
                NexmoCallHandler.IN_APP,
                object : NexmoRequestListener<NexmoCall?> {
                    override fun onSuccess(p0: NexmoCall?) {
                        view.onCallSuccess(p0)
                    }

                    override fun onError(p0: NexmoApiError) {
                        view.onCallError(p0)
                    }
                })
        }
    }

    fun incomingCall() {
        nexmoClient.addIncomingCallListener { nexmoCall ->
            if (currentCall == null) {
                view.onIncomingCall(nexmoCall)
            }
        }
    }


}
