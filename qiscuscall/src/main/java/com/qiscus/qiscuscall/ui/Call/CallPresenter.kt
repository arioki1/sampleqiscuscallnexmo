package com.qiscus.qiscuscall.ui.Call

import com.nexmo.client.NexmoCall
import com.nexmo.client.request_listener.NexmoApiError
import com.nexmo.client.request_listener.NexmoRequestListener
import com.qiscus.qiscuscall.currentCall


class CallPresenter(
    val view: CallView
) {
    fun endCall() {
        currentCall?.hangup(object : NexmoRequestListener<NexmoCall> {
            override fun onSuccess(call: NexmoCall?) {
                currentCall = null
                view.endCallSuccess(call)
            }

            override fun onError(nexmoApiError: NexmoApiError) {
              view.showError(nexmoApiError)
            }

        })
    }

}
