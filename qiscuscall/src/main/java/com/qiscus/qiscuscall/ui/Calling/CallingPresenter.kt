package com.qiscus.qiscuscall.ui.Calling

import com.nexmo.client.NexmoCall
import com.nexmo.client.request_listener.NexmoApiError
import com.nexmo.client.request_listener.NexmoRequestListener
import com.qiscus.qiscuscall.currentCall

class CallingPresenter(
    val view: CallingView
) {
    fun endCall() {
        currentCall?.hangup(object : NexmoRequestListener<NexmoCall> {
            override fun onSuccess(call: NexmoCall?) {
                currentCall = null
                view.endCall()
            }

            override fun onError(nexmoApiError: NexmoApiError) {
                view.logAndShow(nexmoApiError.message)
            }

        })
    }

    fun answer() {
        view.connecting("Connecting...")
        currentCall?.answer(object : NexmoRequestListener<NexmoCall> {
            override fun onError(nexmoApiError: NexmoApiError) {
                view.logAndShow(nexmoApiError.message)
            }

            override fun onSuccess(call: NexmoCall?) {
                view.onCallConnected()
            }
        })
    }


}
