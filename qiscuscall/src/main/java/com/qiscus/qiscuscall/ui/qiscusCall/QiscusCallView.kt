package com.qiscus.qiscuscall.ui.qiscusCall

import com.nexmo.client.NexmoCall
import com.nexmo.client.NexmoUser
import com.nexmo.client.request_listener.NexmoApiError


interface QiscusCallView {
    fun loginSuccess(p0: NexmoUser?)
    fun lonError(p0: NexmoApiError)
    fun onCallSuccess(p0: NexmoCall?)
    fun onCallError(p0: NexmoApiError)
    fun onIncomingCall(nexmoCall: NexmoCall?)
}
